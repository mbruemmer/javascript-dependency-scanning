#!/usr/bin/env python3

import json
import csv
import re
import yaml
import collections
import argparse
import markdown
import xml.etree.ElementTree as ET
from dateutil.parser import *
from dateutil.relativedelta import *
import glob
from os import path, walk
import unicodedata

# https://stackoverflow.com/questions/7204805/dictionaries-of-dictionaries-merge/7205107#7205107
def merge_dicts(a, b, path=None):
    "merges b into a"
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dicts(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass  # same leaf value
            elif isinstance(a[key], list) and isinstance(b[key], list):
                a[key].extend(b[key])
            else:
                raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a


def merge_post(release_post_dir):
    features = {}

    for feature_file in glob.glob(path.join(release_post_dir, '*.yml')):
        print("Reading file " + feature_file)
        try:
            with open(feature_file, 'r') as content:
                features = merge_dicts(features, yaml.safe_load(content))
        except:
            print("Can not merge data from file: %s" % feature_file)

    return features


def normalizeVersionString(version):
    version = version.split("_")
    major = version[0]
    minor = version[1]
    if len(major) < 2:
        major = "0" + major
    if len(minor) < 2:
        minor = "0" + minor
    return "%s_%s" % (major, minor)


def clean_description(desc_string):
    desc_string = re.sub("<img.*>", "", desc_string)
    # desc_string = desc_string.replace("\n"," ")
    md = markdown.Markdown()
    desc_string = md.convert(desc_string)
    return desc_string

def make_deep_link(feature_name, date, version):
    date = date.split(" ")[0].split("-")
    year = date[0]
    month = date[1]
    day = date[2]
    version = version.split("_")
    slug_version = []
    for versionpart in version:
        if versionpart.startswith("0") and len(versionpart) == 2:
            slug_version.append(versionpart[1:])
        else:
            slug_version.append(versionpart)
    slug_version = "-".join(slug_version)
    deeplink = "https://about.gitlab.com/releases/" + year + "/" + month + "/" + day + "/gitlab-" + slug_version + "-released/"
    name_slug = unicodedata.normalize('NFKD', feature_name).encode('ascii', 'ignore').decode('ascii')
    name_slug = re.sub('[^\w\s-]', '', name_slug).strip().lower().replace(" ","-").replace("_","")
    #deeplink += "#" + feature_name.lower().replace(".","").replace(" ","-").replace("_","").replace("\"","").replace(",","")
    deeplink += "#" + name_slug
    return deeplink

def reformat_features(op):
    feature_data = []
    for release in op:
        date = get_release_date("Gitlab " + release.replace("_","."))
        categories = op[release]

        if "primary" not in categories["features"]:
            print("No primary found in %s. Skipping" % release)
            continue
        for category in categories["features"]:
            for feature in categories["features"][category]:
                feature["deeplink"] = make_deep_link(feature["name"], date, release)
                feature["rank"] = category
                feature["version"] = release
                feature["description"] = clean_description(feature["description"])
                feature_data.append(feature)
        # tertiary top level category starting 13.11, containing bug fixes and improvements
        if "tertiary" in categories:
            for feature in categories["tertiary"]:
                if feature["name"] == "Service bulletins":
                    print("Skipping bulletins")
                    continue
                feature["rank"] = "tertiary"
                feature["version"] = release
                feature["description"] = clean_description(feature["description"])
                feature_data.append(feature)
    return feature_data


def normalize_bugs_improvements(features):
    split_features = []
    for feature in features.copy():
        version = feature["version"].split("_")
        is_new_format = False
        if int(version[0]) > 11:
            is_new_format = True
        elif int(version[0]) == 11 and int(version[1]) > 5:
            is_new_format = True
        if not feature.get("stage") and is_new_format:
            if "gitlab runner" in feature["name"].lower():
                feature["stage"] = "verify"
            elif "omnibus improvements" in feature["name"].lower():
                feature["stage"] = "enablement"
            elif "chart improvements" in feature["name"].lower():
                feature["stage"] = "enablement"
            elif "geo improvements" in feature["name"].lower():
                feature["stage"] = "enablement"
            elif "bug fixes" in feature["name"].lower():
                feature["stage"] = "bug fixes"
                split = split_feature_list(feature, "bug fixes")
                if split:
                    split_features.extend(split)
                    features.remove(feature)
            elif "performance improvements" in feature["name"].lower():
                feature["stage"] = "performance"
                split = split_feature_list(feature, "performance")
                if split:
                    split_features.extend(split)
                    features.remove(feature)
            elif "usability improvements" in feature["name"].lower():
                feature["stage"] = "usability"
                split = split_feature_list(feature, "usability")
                if split:
                    split_features.extend(split)
                    features.remove(feature)
    return (features, split_features)


def split_feature_list(feature, feature_type):
    split_features = []
    description = feature["description"].replace("<code>", "").replace("</code>", "")
    html = "<root>" + description + "</root>"
    try:
        root = ET.fromstring(html)
        feature_links = root.findall("./ul/li/a")
        for feature_link in feature_links:
            split_feature = {"name": feature_link.text, "stage": feature_type,
                             "available_in": ["core", "premium", "ultimate"], "issue_url": feature_link.attrib["href"],
                             "description": "", "version": feature["version"]}
            split_features.append(split_feature)
    except Exception:
        errormsg = "[ERROR] Could not parse feature: %s in %s." % (feature["title"], feature["version"])
        print(errormsg)
    return split_features


def reformat_deprecations(op):
    feature_data = []
    for release in op:
        # don't parse these after 14.4 because they moved into docs
        if release == "14_05":
            return feature_data
        categories = op[release]
        if "deprecations" in categories:
            for feature in categories["deprecations"]:
                feature["version"] = release
                feature["due"] = clean_date(feature["due"])
                feature["description"] = clean_description(feature["description"])
                feature_data.append(feature)
    return feature_data


def clean_date(datestring):
    try:
        return str(parse(datestring))
    except:
        # people are referencing a date using a gitlab release version string
        return get_release_date(datestring)


def get_release_date(versionString):
    print("Computing date from version string %s " % versionString)
    major_dates = {"9":"2017-03-22","10":"2017-09-22","11":"2018-06-22","12":"2019-06-22","13":"2020-05-22","14":"2021-06-22", "15":"2022-05-22"}
    # cut off the gitlab part first
    if "." not in versionString:
        return None
    versionString = versionString.split(" ")[1]
    major = versionString.split(".")[0]
    if major.startswith("0"):
        major = major[1:]
    minor = versionString.split(".")[1]
    if major in major_dates:
        majordate = major_dates[major]
        date = parse(majordate) + relativedelta(months=+int(minor))
        return str(date)
    else:
        # fall back to computing from latest release. This will get inaccurate at some point
        majordate = major_dates["14"]
        major_delta = int(major) - 14
        month_delta = major_delta * 12 + int(minor)
        date = parse(majordate) + relativedelta(months=+month_delta)
        return str(date)


def make_feature_row(version, rank, feature):
    row = []
    if "available_in" not in feature:
        return row
    row.append(version)
    row.append(rank)
    row.append(feature["name"])
    row.append(",".join(feature["available_in"]))
    # markdown to html
    row.append(clean_description(feature["description"]))

    links = ""
    if "documentation_link" in feature:
        if feature["documentation_link"] is not None:
            links += "<a href=" + feature["documentation_link"] + ">Documentation</a><br>"
    if "issue_url" in feature:
        if isinstance(feature["issue_url"], list):
            for issuelink in feature["issue_url"]:
                links += "<a href=" + issuelink + ">Issue</a><br>"
        else:
            links += "<a href=" + feature["issue_url"] + ">Issue</a><br>"
    if "video" in feature:
        links += "<a href=" + feature["video"] + ">Video</a><br>"
    row.append(links)

    if "stage" in feature:
        row.append(feature["stage"])
    else:
        row.append("")

    if "categories" in feature:
        row.append(",".join(feature["categories"]))
    else:
        row.append("")
    return row


parser = argparse.ArgumentParser(description='Crawl and report on GitLab features')
parser.add_argument('-c', '--cached', help="Use local cache file for testing.", action="store_true")
args = parser.parse_args()

__dirname__ = path.dirname(path.abspath(__file__))
target_dir = path.join(__dirname__, '..', 'public')

if args.cached:
    features = []
    with open(path.join(target_dir, "features.json"), "r") as featurefile:
        features = json.load(featurefile)
        features = normalize_bugs_improvements(features)
    with open(path.join(target_dir, 'features_split.json'), 'w') as outfile:
        json.dump(features[0], outfile, indent=4)
    with open(path.join(target_dir, 'bugs_improvements.json'), 'w') as outfile:
        json.dump(features[1], outfile, indent=4)
else:
    all_posts = {}

    release_posts_dir = path.join(__dirname__, '..', 'data', 'release_posts')

    # Iterating over newer release post folders
    for release_post_dir in walk(release_posts_dir):
        basename = path.basename(release_post_dir[0])
        if re.search("[0-9]+_[0-9]+", basename):
            version = normalizeVersionString(path.basename(release_post_dir[0]))
            print(version + " " + release_post_dir[0])
            all_posts[version] = merge_post(release_post_dir[0])

    # Iterating over older release post ymls
    for release_post in glob.glob(path.join(release_posts_dir, '*_released.yml')):
        print("Reading file " + release_post)
        version = re.findall(r'gitlab_[0-9]+_[0-9]+', release_post)
        if version:
            with open(release_post, 'r') as content:
                version = normalizeVersionString(version[0].replace("gitlab_", ""))
                all_posts[version] = yaml.safe_load(content)
        else:
            print("No version string in %s, skipping." % release_post)

    op = collections.OrderedDict(sorted(all_posts.items()))

    header = ["version", "rank", "name", "available_in", "description", "links", "stage", "categories"]

    features = reformat_features(op)

    with open(path.join(target_dir, 'features.json'), 'w') as outfile:
        json.dump(features, outfile, indent=4)

    split_features = normalize_bugs_improvements(features)
    with open(path.join(target_dir, 'features_split.json'), 'w') as outfile:
        json.dump(split_features[0], outfile, indent=4)
    with open(path.join(target_dir, 'bugs_improvements.json'), 'w') as outfile:
        json.dump(split_features[1], outfile, indent=4)

    with open(path.join(target_dir, 'deprecations.json'), 'w') as outfile:
        json.dump(reformat_deprecations(op), outfile, indent=4)

    rows = []
    version_order = set()
    for version in op:
        # guard against accidental adding of version not yet released
        post = op[version]
        if "primary" not in post["features"]:
            print("ERROR: Primary post not found, skipping %s" % str(version))
            continue
        version_order.add(version)
        
        # as of 12.9, top is not used consistently any more
        if "top" in post["features"]:
            for feature in post["features"]["top"]:
                rows.append(make_feature_row(version, "top", feature))
        for feature in post["features"]["primary"]:
            rows.append(make_feature_row(version, "primary", feature))
        for feature in post["features"]["secondary"]:
            rows.append(make_feature_row(version, "secondary", feature))
        if post.get("tertiary"):
            for feature in post.get("tertiary"):
                row = make_feature_row(version, "tertiary", feature)
                rows.append(row)

    with open(path.join(target_dir, "features.csv"), "w") as featurefile:
        featurewriter = csv.writer(featurefile, delimiter='\t', quoting=csv.QUOTE_MINIMAL)
        featurewriter.writerow(header)
        for row in rows:
            featurewriter.writerow(row)
