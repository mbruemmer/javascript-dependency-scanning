#!/usr/bin/env node
const factory = require("gitlab-api-async-iterator");
const { join, dirname } = require("path");
const { writeFile, mkdir, stat, copyFile } = require("fs").promises;
const asyncPool = require("../lib/async-pool");

const { GitLabAPI, GitLabAPIIterator } = factory();

const targetDir = join(__dirname, "..");

const BLOB_CACHE_PATH = join(__dirname, "..", "blob_cache");

const GITLAB_ID = "7764";

async function fileExists(path) {
  try {
    await stat(path);
    return true;
  } catch (e) {
    return false;
  }
}

async function downloadFileIfNecessary(file) {
  const { type, name, path, id } = file;

  if (type === "blob") {
    const cachePath = join(BLOB_CACHE_PATH, id);
    const targetPath = join(targetDir, path);
    await mkdir(dirname(targetPath), { recursive: true });
    if (await fileExists(cachePath)) {
      console.log(`Loading ${path} via Cache`);
      await copyFile(cachePath, targetPath);
    } else {
      console.log(`Loading ${path} via API`);
      const { data } = await GitLabAPI.get(
        `/projects/${GITLAB_ID}/repository/blobs/${id}`
      );
      const buffer = Buffer.from(data.content, "base64");
      await writeFile(targetPath, buffer, "utf-8");
      await writeFile(cachePath, buffer, "utf-8");
    }

    return { name };
  }

  return [];
}

async function main() {
  await mkdir(BLOB_CACHE_PATH, { recursive: true });

  let stack = ["data/release_posts"];

  const releasePostItems = [];

  do {
    const path = stack.shift();

    const fileIterator = new GitLabAPIIterator(
      `/projects/${GITLAB_ID}/repository/tree`,
      { per_page: 100, path }
    );

    for await (const entry of fileIterator) {
      const { type, path } = entry;
      if (type === "blob" && path.endsWith(".yml")) {
        console.log(`Found ${path}`);
        releasePostItems.push(entry);
      }

      if (type === "tree") {
        stack.push(path);
      }
    }
  } while (stack.length);

  await asyncPool(20, releasePostItems, downloadFileIfNecessary);

  console.log(`Loaded ${releasePostItems.length} items`);
}

main()
  .then(() => {
    console.log("success");
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
