#!/usr/bin/env node
const factory = require("gitlab-api-async-iterator");
const axios = require("axios");
const { join } = require("path");
const { writeFileSync, readFileSync } = require("fs");
const cheerio = require("cheerio");
const { DateTime } = require("luxon");
const asyncPool = require("../lib/async-pool");

const { GitLabAPI, GitLabAPIIterator } = factory();

const GITLAB_ID = "278964";

const MONTH_AGO = DateTime.now().plus({ months: -1 });

const DOT_COM_FILE = join(__dirname, "..", "public", "dot_com.json");

function parseRelatedIssues(description_html) {
  const $ = cheerio.load(description_html);
  const links = [...$("a")].flatMap((link) => {
    const href = $(link).attr("href") || "";

    if (
      href.startsWith("https://gitlab.com/") &&
      /\/issues\/[0-9]+/.test(href)
    ) {
      const parsed = new URL(href);
      parsed.search = "";
      parsed.hash = "";
      return parsed.toString();
    }

    return [];
  });
  return [...new Set(links)];
}

function parseHTML(description_html) {
  const $ = cheerio.load(description_html, null, false);

  $("a.anchor").remove();

  $("[data-sourcepos]").each(function () {
    $(this).attr("data-sourcepos", null);
  });

  $("a").each(function () {
    const href = $(this).attr("href");
    if (href) {
      $(this).attr(
        "href",
        href.startsWith("/") ? `https://gitlab.com${href}` : href
      );
    }
  });

  $("img.lazy").each(function () {
    const src = $(this).attr("data-src") || $(this).attr("src") || "";
    if (src) {
      $(this).attr("loading", "lazy");
      $(this).attr(
        "src",
        src.startsWith("/") ? `https://gitlab.com${src}` : src
      );
    }
  });

  $("video").each(function () {
    const src = $(this).attr("data-src") || $(this).attr("src") || "";
    if (src) {
      $(this).attr(
        "src",
        src.startsWith("/") ? `https://gitlab.com${src}` : src
      );
    }
  });

  return $.html();
}

async function processMergeRequest(mr) {
  const api_url = `/projects/${GITLAB_ID}/merge_requests/${mr.iid}?render_html=true`;

  console.log(`Loading details for ${mr.web_url}`);

  const [details, deploy_status] = await Promise.all([
    GitLabAPI.get(api_url),
    // Ugly hack
    axios.get(
      `https://gitlab.com/gitlab-org/gitlab/-/merge_requests/${mr.iid}/ci_environments_status?environment_target=merge_commit`
    ),
  ]);

  const { data: mr_details } = details;

  const { data: deployments } = deploy_status;

  const deployed_to_prod =
    deployments.find((x) => x.name === "gprd")?.deployed_at ||
    deployments.find((x) => x.name === "db/gprd")?.deployed_at ||
    deployments.find((x) => x.name === "gprd-cny")?.deployed_at;

  if (!deployed_to_prod) {
    console.log(`Deployment info missing, skipping ${mr_details.web_url}`);
    return [];
  }

  const description_html = parseHTML(mr_details.description_html);

  const related_issues = parseRelatedIssues(description_html);

  const { id, title, web_url, labels, references } = mr_details;

  return {
    type: "MR",
    id: `MR-${id}`,
    title,
    web_url,
    labels,
    description_html,
    deployed_to_prod,
    related_issues,
    reference: references.full,
  };
}

async function processIssue([issue_url, val]) {
  console.log(`Processing ${issue_url}`);
  const match = issue_url.match(
    /https:\/\/gitlab.com\/(.+)\/-\/issues\/([0-9]+)/
  );

  if (!match) {
    return [];
  }
  const { related_mrs, deployed_to_prod } = val;
  const [, path, issue_iid] = match;
  const api_url = `/projects/${encodeURIComponent(path)}/issues/${issue_iid}`;
  let issue_details;
  try {
    ({ data: issue_details } = await GitLabAPI.get(api_url));
  } catch (e) {
    console.log(`Could not get info on ${issue_url}`);
    return [];
  }

  const {
    id,
    title,
    web_url,
    labels,
    description,
    references,
    confidential,
  } = issue_details;

  if (confidential) {
    return [];
  }

  let html;

  try {
    const res = await GitLabAPI.post("/markdown", {
      text: description,
      gfm: true,
      project: path,
    });

    html = parseHTML(res.data.html);
  } catch (e) {
    console.log("Could not parse description of " + web_url);
    html = `<strong>Could not parse:</strong><pre>${description}</pre>`;
  }

  const description_html = html;

  return {
    type: "ISSUE",
    id: `I-${id}`,
    title,
    web_url,
    labels,
    description_html,
    deployed_to_prod,
    related_mrs,
    reference: references.full,
  };
}

async function getDeployedMRs(deployedAfter = MONTH_AGO.toISODate()) {
  console.log(deployedAfter);
  const deployed_after = DateTime.fromISO(deployedAfter);

  console.log(`Start querying data, starting from ${deployed_after.toISO()}`);

  const MRIterator = new GitLabAPIIterator(
    `/projects/${GITLAB_ID}/merge_requests`,
    {
      state: "merged",
      environment: "gprd",
      deployed_after: deployed_after.toISO({ suppressMilliseconds: true }),
    }
  );

  return asyncPool(10, MRIterator, processMergeRequest);
}

async function getRelatedIssues(mrs) {
  const issueMap = mrs
    .flatMap((mr) => {
      const relatedIssues = mr?.related_issues || [];
      return relatedIssues.map((issue) => [
        issue,
        mr.web_url,
        mr.deployed_to_prod,
      ]);
    })
    .reduce((acc, curr) => {
      const [issue, mr, dep] = curr;
      const related_mrs = [...(acc[issue]?.related_mrs || []), mr];
      const deployed_to_prod =
        acc[issue]?.deployed_to_prod || "0000-00-00T00:00:00Z";
      acc[issue] = {
        related_mrs,
        deployed_to_prod: dep > deployed_to_prod ? dep : deployed_to_prod,
      };
      return acc;
    }, {});

  return asyncPool(10, Object.entries(issueMap), processIssue);
}

async function main() {
  let oldMRs;
  try {
    oldMRs = JSON.parse(readFileSync(DOT_COM_FILE, "utf-8"));
  } catch (e) {
    oldMRs = [];
  }

  const lastDeployment = oldMRs
    .flatMap((issueOrMR) => {
      const { deployed_to_prod } = issueOrMR;
      if (deployed_to_prod) {
        return deployed_to_prod;
      }
      console.log(issueOrMR);
      return [];
    })
    .sort()
    .reverse()[0];

  const newMrs = await getDeployedMRs(lastDeployment);

  const gitLabComIssues = await getRelatedIssues(newMrs);

  const items = [oldMRs, newMrs, gitLabComIssues].flat().reduce((acc, curr) => {
    if (DateTime.fromISO(curr.deployed_to_prod) < MONTH_AGO) {
      console.log(
        `${curr.deployed_to_prod} happened more than a month ago, dropping ${curr.web_url}`
      );
      return acc;
    }
    acc[curr.id] = curr;
    return acc;
  }, {});

  writeFileSync(DOT_COM_FILE, JSON.stringify(Object.values(items)));
}

main()
  .then(() => {
    console.log("success");
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
