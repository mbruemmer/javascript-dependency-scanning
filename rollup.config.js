import path from "path";
import alias from "@rollup/plugin-alias";
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import replace from "@rollup/plugin-replace";
import copy from "rollup-plugin-copy";
import { terser } from "rollup-plugin-terser";
import vue from "rollup-plugin-vue";
import moment from "moment";

// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;

const rootDir = path.resolve(__dirname);

export default {
  input: "src/main.js",
  output: {
    file: "public/js/bundle.js",
    format: "umd", // immediately-invoked function expression — suitable for <script> tags
    sourcemap: true,
  },
  plugins: [
    copy({
      targets: [
        {
          src: path.join(
            rootDir,
            "node_modules/bootstrap/dist/css/bootstrap.min.css"
          ),
          dest: "public/css/",
        },
        {
          src: path.join(
            rootDir,
            "node_modules/bootstrap/dist/css/bootstrap.min.css.map"
          ),
          dest: "public/css/",
        },
        {
          src: path.join(
            rootDir,
            "node_modules/vue-multiselect/dist/vue-multiselect.min.css"
          ),
          dest: "public/css/",
        },
        {
          src: path.join(
            rootDir,
            "node_modules/vue-multiselect/dist/vue-multiselect.min.css.map"
          ),
          dest: "public/css/",
        },
        {
          src: path.join(
            rootDir,
            "node_modules/dc/dist/style/dc.min.css"
          ),
          dest: "public/css/",
        },
      ],
    }),
    replace({
      "process.env.NODE_ENV": '"production"',
      LAST_BUILT: moment().toISOString(),
    }),
    alias({
      vue: require.resolve("vue/dist/vue.runtime.esm.js"),
    }),
    resolve(), // tells Rollup how to find date-fns in node_modules
    commonjs(), // converts date-fns to ES modules
    vue({
      needMap: false,
    }),
    production && terser(), // minify, but only in production
  ],
};
