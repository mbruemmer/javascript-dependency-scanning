// Based of: https://github.com/rxaviers/async-pool/blob/1e7f18aca0bd724fe15d992d98122e1bb83b41a4/lib/es7.js
async function asyncPool(poolLimit, iterator, iteratorFn) {
  const ret = [];
  const executing = [];
  for await (const item of iterator) {
    const p = Promise.resolve().then(() => iteratorFn(item, iterator));
    ret.push(p);

    if (poolLimit <= iterator.length) {
      const e = p.then(() => executing.splice(executing.indexOf(e), 1));
      executing.push(e);
      if (executing.length >= poolLimit) {
        await Promise.race(executing);
      }
    }
  }
  return Promise.all(ret);
}

module.exports = asyncPool;
